import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { InspeccionPage } from  '../pages/inspeccion/inspeccion'
import { ReportesPage } from '../pages/reportes/reportes'
import { InstruccionesPage } from '../pages/instrucciones/instrucciones'
import { AcercaPage } from '../pages/acerca/acerca'
import { ControlCalidadPage } from '../pages/controlCalidad/controlCalidad';
import { PuentesUsoPage } from '../pages/puentesUso/puentesUso'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AmpollamientoPage } from '../modals/ampollamiento/ampollamiento'
import {OxidacionPage } from '../modals/oxidacion/oxidacion'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    InspeccionPage,
    ReportesPage,
    InstruccionesPage,
    AcercaPage,
    ControlCalidadPage,
    PuentesUsoPage,
    AmpollamientoPage,
    OxidacionPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    InspeccionPage,
    ReportesPage,
    InstruccionesPage,
    AcercaPage,
    ControlCalidadPage,
    PuentesUsoPage,
    AmpollamientoPage,
    OxidacionPage
  ],
  providers: [
    StatusBar,
    File,
    FileOpener,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
