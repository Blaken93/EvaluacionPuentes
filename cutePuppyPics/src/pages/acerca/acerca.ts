import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-instrucciones',
  templateUrl: 'acerca.html'
})
export class AcercaPage {

  constructor(public navCtrl: NavController) {

  }

}
