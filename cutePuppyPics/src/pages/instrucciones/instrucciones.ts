import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-instrucciones',
  templateUrl: 'instrucciones.html'
})
export class InstruccionesPage {

  constructor(public navCtrl: NavController) {

  }

}
