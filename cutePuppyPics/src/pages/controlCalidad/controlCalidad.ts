import {Component} from '@angular/core';
import {NavController,AlertController} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import * as pdfmake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
@Component({
  selector: 'page-controlCalidad',
  templateUrl: 'controlCalidad.html'
})


export class ControlCalidadPage {
  inspeccionEquipos : FormGroup;
  condicionesAmbientales : FormGroup;
  preparacionSuperficie: FormGroup;
  almacenamientoPintura: FormGroup;
  aplicacionPintura:FormGroup;
  medicionesPintura: FormGroup;
  adherenciaRecubrimiento:FormGroup;

  constructor(public navCtrl: NavController,private file: File, private fileOpener: FileOpener,private  alertCtrl: AlertController) {
    this.inspeccionEquipos = new FormGroup({
      calibradosCertificados: new FormControl('',Validators.required),
      aireComprimido : new FormControl('',Validators.required),
      boquillaIndicada : new FormControl('',Validators.required),
      presionAire : new FormControl('',Validators.required),
      EPPCompleto : new FormControl('',Validators.required),
      heramientasManuales : new FormControl('',Validators.required),
      herramientasElectricas : new FormControl('',Validators.required)
    });

    this.condicionesAmbientales = new FormGroup({
      temperatura: new FormControl('',Validators.required),
      humedad : new FormControl('',Validators.required),
      puntoRocio : new FormControl('',Validators.required)
    });
    this.preparacionSuperficie = new FormGroup({
      superficieLibre: new FormControl('',Validators.required),
      imperfecciones : new FormControl('',Validators.required),
      limpieza : new FormControl('',Validators.required),
      condicionesAmbientales : new FormControl('',Validators.required),
    });
    this.almacenamientoPintura = new FormGroup({
      almacenadoAdecuadamente: new FormControl('',Validators.required),
      etiquetadoCorrectamente : new FormControl('',Validators.required),
      fichaTecnica : new FormControl('',Validators.required),
      condicionesAmbientales : new FormControl('',Validators.required),
    });
    this.aplicacionPintura = new FormGroup({
      aplicacionCondFabricante: new FormControl('',Validators.required),
      aplicacionCondAmbientales : new FormControl('',Validators.required),
      propagacion : new FormControl('',Validators.required),
      defectos : new FormControl('',Validators.required),
    });
    this.medicionesPintura = new FormGroup({
      espesoresPelHumeda: new FormControl('',Validators.required),
      espesoresPinturas : new FormControl('',Validators.required),
      curado : new FormControl('',Validators.required)
    });
    this.adherenciaRecubrimiento = new FormGroup({
      pruebaFavorable: new FormControl('',Validators.required)
    });



  }

  private cumpleFill(listString,value){
    if (value=="Si"){
      listString[2]='X';
    }
    else if (value=="No"){
      listString[3]='X';
    }
    else if (value=="NA"){
      listString[4]='X';
    }
    return listString;
  }


  public testPdf() {
    var dd = { content: [{text: 'Control Calidad', style: 'header'},
      {
        style: 'tableExample',
        table: {
          widths: [15, 200, 20,20,22,80,115],
          headerRows: 2,
          // keepWithHeaderRows: 1,
          body: [
            [{text: 'N', style: 'tableHeader',  alignment: 'center'}, {text: 'Parametros', style: 'tableHeader', alignment: 'center'}, {text: 'Cumple', colSpan: 3,style: 'tableHeader', alignment: 'center'},{},{}, {text: 'Referencia: criterio de aceptación', style: 'tableHeader', alignment: 'center'},{text: 'Observaciones', style: 'tableHeader', colSpan: 1, alignment: 'center'}],
            ['','' ,'Si','No','N/A','',''],
            [{text: 'INSPECCION DE EQUIPOS', style: 'tableHeader', alignment: 'left', colSpan:7},'' ,'','','','',''],
            this.cumpleFill(['','Los equipos a utilizar se encuentran calibrados, certificados y se encuentran en buen funcionamiento' ,'','','','NACE',''],this.inspeccionEquipos.value.calibradosCertificados),
            this.cumpleFill(['','Los equipos de aire comprimido funcionan adecuadamente' ,'','','','NACE',''],this.inspeccionEquipos.value.aireComprimido),
            this.cumpleFill(['','La boquilla del equipo es la indicada, su diámetro y estado' ,'','','','NACE',''],this.inspeccionEquipos.value.boquillaIndicada),
            this.cumpleFill(['','La presión del aire comprimido es la correcta' ,'','','','NACE',''],this.inspeccionEquipos.value.presionAire),
            this.cumpleFill(['','El personal cuenta con el EPP completo y acorde a la actividad a realizar' ,'','','','NACE',''],this.inspeccionEquipos.value.EPPCompleto),
            this.cumpleFill(['','Las herramientas manuales están en buen estado para su utilización' ,'','','','NACE',''], this.inspeccionEquipos.value.heramientasManuales),
            this.cumpleFill(['','Las herramientas eléctricas están en buen estado para su utilización' ,'','','','NACE',''],this.inspeccionEquipos.value.herramientasElectricas),
            [{text: 'CONDICIONES AMBIENTALES', style: 'tableHeader', alignment: 'left', colSpan:7},'' ,'','','','',''],
            this.cumpleFill( ['','La temperatura del sustrato es aceptable' ,'','','','Especificación técnica',''],this.condicionesAmbientales.value.temperatura),
            this.cumpleFill( ['','La humedad relativa es aceptable' ,'','','','Especificación técnica',''],this.condicionesAmbientales.value.humedad),
            this.cumpleFill( ['','El punto de rocío es aceptable' ,'','','','Especificación técnica',''],this.condicionesAmbientales.value.puntoRocio),
            [{text: 'PREPARACIÓN DE SUPERFICIE', style: 'tableHeader', alignment: 'left', colSpan:7},'' ,'','','','',''],
            this.cumpleFill(['','La superficie se encuentra libre de polvo, grasa u otro tipo de agentes que impidan una buena adherencia de la pintura' ,'','','','NACE',''],this.preparacionSuperficie.value.superficieLibre),
            this.cumpleFill(['','Las imperfecciones del sustrato no afectan la adherencia de la pintura' ,'','','','NACE',''],this.preparacionSuperficie.value.imperfecciones),
            this.cumpleFill( ['','Se realiza una limpieza según la especificación técnica' ,'','','','Especificación técnica',''],this.preparacionSuperficie.value.limpieza),
            this.cumpleFill(['','Las condiciones ambientales cumplen según los valores mínimos de aceptación' ,'','','','Especificación técnica',''],this.preparacionSuperficie.value.condicionesAmbientales),
            [{text: 'ALMACENAMIENTO Y PREPARACIÓN DE PINTURA', style: 'tableHeader', alignment: 'left', colSpan:7},'' ,'','','','',''],
            this.cumpleFill( ['','La pintura es almacenada de manera adecuada, en contenedores resistentes y sin peligro de contaminarse' ,'','','','CR-2010',''],this.almacenamientoPintura.value.almacenadoAdecuadamente),
            this.cumpleFill( ['','La pintura está debidamente etiquetada, como mínimo: marca, tipo, peso neto, volumen, % de sólidos, % de compuestos orgánicos volátiles, fabricante' ,'','','','CR-2010',''],this.almacenamientoPintura.value.etiquetadoCorrectamente),
            this.cumpleFill( ['','Se cuenta con la ficha técnica correspondiente a cada pintura' ,'','','','',''],this.almacenamientoPintura.value.fichaTecnica),
            this.cumpleFill( ['','La preparación de la pintura se realiza de acuerdo a las especificaciones del fabricante' ,'','','','',''],this.almacenamientoPintura.value.condicionesAmbientales),
            [{text: 'APLICACIÓN DE LA PINTURA', style: 'tableHeader', alignment: 'left', colSpan:7},'' ,'','','','',''],
            this.cumpleFill(['','Se aplica la pintura según las especificaciones del fabricante' ,'','','','',''],this.aplicacionPintura.value.aplicacionCondFabricante),
            this.cumpleFill(['','Se aplica la pintura acorde a las condiciones ambientales requeridas' ,'','','','Especificación técnica',''],this.aplicacionPintura.value.aplicacionCondAmbientales),
            this.cumpleFill( ['','Se evita propagación de polvo o suciedad que pueda contaminar las capas de recubrimiento' ,'','','','',''],this.aplicacionPintura.value.propagacion),
            this.cumpleFill( ['','Se observan defectos no aceptables en el acabado de pintura' ,'','','','',''],this.aplicacionPintura.value.defectos),
            [{text: 'MEDICION DE ESPESORES  Y CURADO DE PINTURA', style: 'tableHeader', alignment: 'left', colSpan:7},'' ,'','','','',''],
            this.cumpleFill(['','Se realizan las mediciones de espesores de película húmeda' ,'','','','SSPC-PA2',''],this.medicionesPintura.value.espesoresPelHumeda),
            this.cumpleFill(['','Los espesores cumplen para cada capa de pintura (película seca)' ,'','','','Especificación técnica',''],this.medicionesPintura.value.espesoresPinturas),
            this.cumpleFill(['','El proceso de curado se realiza según la especificación del fabricante' ,'','','','NACE',''],this.medicionesPintura.value.curado),
            [{text: 'ADHERENCIA DEL RECUBRIMIENTO', style: 'tableHeader', alignment: 'left', colSpan:7},'' ,'','','','',''],
            this.cumpleFill(['','La prueba de adherencia es favorable' ,'','','','ASTM D3359 o ASTM D4541',''],this.adherenciaRecubrimiento.value.pruebaFavorable)

          ]
        }
      }



      ] };
    pdfmake.vfs = pdfFonts.pdfMake.vfs;
    //pdfmake.createPdf(dd).download('ControlCalidad.pdf');
    pdfmake.createPdf(dd).getBlob(buffer => {
      this.file.resolveDirectoryUrl(this.file.externalRootDirectory)
        .then(dirEntry => {
          this.file.getFile(dirEntry, 'test1.pdf', { create: true })
            .then(fileEntry => {
              fileEntry.createWriter(writer => {
                writer.onwrite = () => {
                  this.fileOpener.open(fileEntry.toURL(), 'application/pdf')
                    .then(res => { })
                    .catch(err => {
                      const alert = this.alertCtrl.create({ message: err.message, buttons: ['Ok'] });
                      alert.present();
                    });
                }
                writer.write(buffer);
              })
            })
            .catch(err => {
              const alert = this.alertCtrl.create({ message: err, buttons: ['Ok'] });
              alert.present();
            });
        })
        .catch(err => {
          const alert = this.alertCtrl.create({ message: err, buttons: ['Ok'] });
          alert.present();
        });

    });
  }

}

