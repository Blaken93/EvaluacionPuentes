import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PuentesUsoPage } from '../puentesUso/puentesUso';
import { ControlCalidadPage } from '../controlCalidad/controlCalidad';
import { AlertController } from 'ionic-angular';
@Component({
  selector: 'page-inspeccion',
  templateUrl: 'inspeccion.html'
})
export class InspeccionPage {
  tab1Root = PuentesUsoPage;
  tab2Root = ControlCalidadPage;
  inicio:boolean;
  constructor(public navCtrl: NavController,private alertCtrl: AlertController) {
    this.inicio=false;
  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Desea cambiar de inspeccion',
      message: 'Estas apunto de cambiar de inspeccion \n Desea continuar?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Continuar',
          handler: () => {
            console.log('Buy clicked');
          }
        }
      ]
    });
    if (this.inicio){ alert.present();}
    else {this.inicio=true;}
  }
}
