import { Component } from '@angular/core';
import {NavController, AlertController, ModalController} from 'ionic-angular';
import {FormControl, FormGroup,Validators} from "@angular/forms";
import {AmpollamientoPage } from '../../modals/ampollamiento/ampollamiento'
@Component({
  selector: 'page-puentesUso',
  templateUrl: 'puentesUso.html'
})

export class PuentesUsoPage {
  informacionGeneral : FormGroup;
  informacionControlMantenimiento : FormGroup;
  sistemaProteccionMetalicosLista: Array<{title: string,selected:boolean}>;
  sistemaProteccionLista : Array<{title: string,selected:boolean}>;
  ampollamiento : any;
  oxidaxion: any;
  agrietamiento: any;
  descamacion :any;
  modalData:any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController,public modalCtrl: ModalController) {
    this.informacionGeneral = new FormGroup({
      nombrePuente: new FormControl('',Validators.required),
      localizacionProvincia : new FormControl('',Validators.required),
      localizacionCanton : new FormControl('',Validators.required),
      localizacionDistrito : new FormControl('',Validators.required),
      administradoPor : new FormControl('',Validators.required),
      latitudNorte : new FormControl('',Validators.required),
      latitudOeste : new FormControl('',Validators.required),
      numeroRuta : new FormControl('',Validators.required),
      clasificacion : new FormControl('',Validators.required),
      kilometro : new FormControl('',Validators.required)
    });

    this.informacionControlMantenimiento = new FormGroup({
      fechaUltimoMantenimiento: new FormControl('',Validators.required),
      fechaUltimaPintura : new FormControl('',Validators.required),
      observacionesUltimoMantenimiento : new FormControl(''),
      observacionesUltimaPintura : new FormControl('')
    });

    this.ampollamiento=[];
    this.oxidaxion=[];
    this.agrietamiento=[];
    this.descamacion=[];
    this.sistemaProteccionMetalicosLista = [
      { title: 'Vigas',selected:false},
      { title: 'Cerchas',selected:false},
      { title: 'Barreras',selected:false},
      { title: 'Apoyos',selected:false },
      { title: 'Pilas',selected:false},
      { title: 'Cables',selected:false},
    ];
    this.sistemaProteccionLista = [
      { title: 'Sistema de protección por barrera',selected:false},
      { title: 'Galvanización en frío',selected:false},
      { title: 'No aplica',selected:false}
    ];
  }




  addToLista(lista,data) {

    lista.push(data)
  }



  editAmpollamiento(lista, note,data){
      let index = lista.indexOf(note);

      if(index > -1){
        lista[index] = data;
      }

  }

  deleteNote(lista,note){

    let index = lista.indexOf(note);

    if(index > -1){
      lista.splice(index, 1);
    }
  }

  openAddModal(lista){
    let myModal = this.modalCtrl.create(AmpollamientoPage);
    myModal.onDidDismiss(data => {
       this.addToLista(lista,data);
    });

    myModal.present();
  }

  save(){
    console.log(this.informacionGeneral.value);
  }

}
