import { Component } from '@angular/core';
import { NavController,ModalController } from 'ionic-angular';
import {AmpollamientoPage} from '../../modals/ampollamiento/ampollamiento'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,public modalCtrl:ModalController) {

  }

  openModal(){
    let myModal = this.modalCtrl.create(AmpollamientoPage);
    myModal.present();
  }
}
