import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-ampollamiento',
  templateUrl: 'ampollamiento.html'
})
export class AmpollamientoPage {
  sistemaProteccionLista:any;
  constructor(public navCtrl: NavController, public  viewCtrl: ViewController) {
    this.sistemaProteccionLista = [
      { title: 'Sistema de protección por barrera',selected:false},
      { title: 'Galvanización en frío',selected:false},
      { title: 'No aplica',selected:false}
    ];
  }
  closeModal() {
    let data = { QS:'asd', C:'meh', Zona:"asd" ,PorcentajeArea:"123"};
    this.viewCtrl.dismiss(data);
  }
}
