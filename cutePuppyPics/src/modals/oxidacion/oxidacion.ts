import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-oxidacion',
  templateUrl: 'oxidacion.html'
})
export class OxidacionPage {

  constructor(public navCtrl: NavController, public  viewCtrl: ViewController) {

  }
  closeModal() {
    let data = { GradoOxidacion:'asd', Zona:"asd" ,PorcentajeArea:"123"};
    this.viewCtrl.dismiss(data);
  }
}
